<?php

/*
 * ClientTest.php
 */

namespace AzureSpring\Wxpay;

/**
 * OpensslNonceTest
 */
class OpensslNonceTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function takeOk()
    {
        $nonce = (new OpensslNonce())->take(16);

        $this->assertRegExp('/[\w|-]{16}/', $nonce);
    }
}
