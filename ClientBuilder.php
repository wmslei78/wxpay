<?php

namespace AzureSpring\Wxpay;

use GuzzleHttp\Client as Guzzle;
use Monolog\Logger;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;

class ClientBuilder
{
    /** @var string */
    private $appId;

    /** @var string */
    private $tradeType;

    /** @var string */
    private $merchantId;

    /** @var string */
    private $secret;

    /** @var string|null */
    private $privKey;

    /** @var string|null */
    private $certificate;

    /** @var NonceInterface */
    private $nonce;

    /** @var Guzzle */
    private $guzzle;

    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /** @var LoggerInterface */
    private $logger;

    public static function create()
    {
        return new ClientBuilder();
    }

    /**
     * @param string $appId
     *
     * @return $this
     */
    public function setAppId(string $appId): self
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTradeType(): string
    {
        return $this->tradeType;
    }

    /**
     * @param string $tradeType
     *
     * @return $this
     */
    public function setTradeType(string $tradeType): ClientBuilder
    {
        $this->tradeType = $tradeType;

        return $this;
    }

    /**
     * @param string $merchantId
     *
     * @return $this
     */
    public function setMerchantId(string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    /**
     * @param string $secret
     *
     * @return $this
     */
    public function setSecret(string $secret): self
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @param string|null $privKey
     *
     * @return $this
     */
    public function setPrivKey(?string $privKey): self
    {
        $this->privKey = $privKey;

        return $this;
    }

    /**
     * @param string|null $certificate
     *
     * @return $this
     */
    public function setCertificate(?string $certificate): self
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * @param NonceInterface $nonce
     *
     * @return $this
     */
    public function setNonce(NonceInterface $nonce): self
    {
        $this->nonce = $nonce;

        return $this;
    }

    /**
     * @param Guzzle $guzzle
     *
     * @return $this
     */
    public function setGuzzle(Guzzle $guzzle): self
    {
        $this->guzzle = $guzzle;

        return $this;
    }

    /**
     * @param ResponseFactoryInterface $responseFactory
     *
     * @return $this
     */
    public function setResponseFactory(ResponseFactoryInterface $responseFactory): self
    {
        $this->responseFactory = $responseFactory;

        return $this;
    }

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;

        return $this;
    }

    public function build()
    {
        return new Client(
            $this->appId,
            $this->tradeType,
            $this->merchantId,
            $this->secret,
            $this->certificate,
            $this->privKey,
            $this->nonce ?? new OpensslNonce(),
            $this->guzzle ?? new Guzzle([
                'base_uri' => Client::BASE_URI,
                'timeout' => 30,
            ]),
            $this->responseFactory,
            $this->logger ?? (class_exists(Logger::class) ? new Logger('azurespring/wxpay') : null)
        );
    }
}
