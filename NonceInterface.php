<?php

/*
 * NonceInterface.php
 */

namespace AzureSpring\Wxpay;

/**
 * NonceInterface
 */
interface NonceInterface
{
    /**
     * @param int $len
     *
     * @return string
     */
    public function take(int $len): string;
}
