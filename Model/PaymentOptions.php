<?php

/*
 * Payment.php
 */

namespace AzureSpring\Wxpay\Model;

/**
 * Payment
 */
class PaymentOptions
{
    /* TRADE TYPES */
    const TYPE_JSAPI      = 'JSAPI';
    const TYPE_NATIVE     = 'NATIVE';
    const TYPE_APP        = 'APP';
    const TYPE_WEB        = 'MWEB';

    /* LIMITS */
    const LIMIT_NO_CREDIT = 'no_credit';

    /**
     * out_trade_no - (merchant) order id
     *
     * @var string
     */
    private $tradeNo;

    /**
     * trade_type - trade (agent) type
     *
     * @var string
     */
    private $tradeType;

    /**
     * body - (product) description
     *
     * @var string
     */
    private $synopsis;

    /**
     * detail - (product) detailed description
     *
     * @var string|null
     */
    private $content;

    /**
     * attach - attachment (data)
     *
     * @var string|null
     */
    private $attachment;

    /**
     * @var string|null
     */
    private $productId;

    /**
     * total_fee - payment amount in cent
     *
     * @var int
     */
    private $total;

    /**
     * fee_type - ISO 4217 currency code
     *
     * @var string|null
     */
    private $currencyCode;

    /**
     * limit_pay / no_credit - is credit card acceptable
     *
     * @var bool
     */
    private $creditCardOk;

    /**
     * spbill_create_ip - client ip
     *
     * @var string
     */
    private $ip;

    /**
     * openid - openid, the
     *
     * @var string|null
     */
    private $openId;

    /**
     * time_start - order create-at
     *
     * @var \DateTimeImmutable
     */
    private $startAt;

    /**
     * time_expire - order expire-at
     *
     * @var \DateTimeImmutable
     */
    private $expireAt;

    /**
     * notify_url - async callback
     *
     * @var string
     */
    private $notifyUrl;

    /**
     * Constructor.
     *
     * @param string $tradeNo
     * @param string $synopsis
     * @param int    $total
     * @param string $ip
     * @param string $notifyUrl
     *
     * @throws \Exception
     */
    public function __construct(string $tradeNo, string $synopsis, int $total, string $ip, string $notifyUrl)
    {
        $this->tradeNo      = $tradeNo;
        $this->tradeType    = self::TYPE_JSAPI;
        $this->synopsis     = $synopsis;
        $this->total        = $total;
        $this->creditCardOk = true;
        $this->ip           = $ip;
        $this->startAt      = new \DateTimeImmutable();
        $this->expireAt     = new \DateTimeImmutable('+2 hours');
        $this->notifyUrl    = $notifyUrl;
    }

    /**
     * @return string
     */
    public function getTradeNo(): string
    {
        return $this->tradeNo;
    }

    /**
     * @param string $tradeNo
     *
     * @return $this
     */
    public function setTradeNo(string $tradeNo): self
    {
        $this->tradeNo = $tradeNo;

        return $this;
    }

    /**
     * @return string
     */
    public function getTradeType(): string
    {
        return $this->tradeType;
    }

    /**
     * @param string $tradeType
     *
     * @return $this
     */
    public function setTradeType(string $tradeType): self
    {
        $this->tradeType = $tradeType;

        return $this;
    }

    /**
     * @return string
     */
    public function getSynopsis(): string
    {
        return $this->synopsis;
    }

    /**
     * @param string $synopsis
     *
     * @return $this
     */
    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttachment(): ?string
    {
        return $this->attachment;
    }

    /**
     * @param string|null $attachment
     *
     * @return $this
     */
    public function setAttachment(?string $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string|null $productId
     *
     * @return $this
     */
    public function setProductId(?string $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string|null $currencyCode
     *
     * @return $this
     */
    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCreditCardOk(): bool
    {
        return $this->creditCardOk;
    }

    /**
     * @param bool $creditCardOk
     *
     * @return $this
     */
    public function setCreditCardOk(bool $creditCardOk): self
    {
        $this->creditCardOk = $creditCardOk;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return $this
     */
    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOpenId(): ?string
    {
        return $this->openId;
    }

    /**
     * @param string|null $openId
     *
     * @return $this
     */
    public function setOpenId(?string $openId): self
    {
        $this->openId = $openId;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getStartAt(): \DateTimeImmutable
    {
        return $this->startAt;
    }

    /**
     * @param \DateTimeImmutable $startAt
     *
     * @return $this
     */
    public function setStartAt(\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getExpireAt(): \DateTimeImmutable
    {
        return $this->expireAt;
    }

    /**
     * @param \DateTimeImmutable $expireAt
     *
     * @return $this
     */
    public function setExpireAt(\DateTimeImmutable $expireAt): self
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return string[]
     */
    public function dispose()
    {
        return [
            'body'             => $this->getSynopsis(),
            'detail'           => $this->getContent(),
            'attach'           => $this->getAttachment(),
            'out_trade_no'     => $this->getTradeNo(),
            'fee_type'         => $this->getCurrencyCode(),
            'total_fee'        => $this->getTotal(),
            'spbill_create_ip' => $this->getIp(),
            'time_start'       => $this->getStartAt()->setTimezone(new \DateTimeZone('Asia/Shanghai'))->format('YmdHis'),
            'time_expire'      => $this->getExpireAt()->setTimezone(new \DateTimeZone('Asia/Shanghai'))->format('YmdHis'),
            'notify_url'       => $this->getNotifyUrl(),
            'trade_type'       => $this->getTradeType(),
            'product_id'       => $this->getProductId(),
            'limit_pay'        => $this->isCreditCardOk() ? null : self::LIMIT_NO_CREDIT,
            'openid'           => $this->getOpenId(),
        ];
    }
}
