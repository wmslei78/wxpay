<?php

/*
 * RefundFragment.php
 */
namespace AzureSpring\Wxpay\Model;

/**
 * RefundFragment
 */
class RefundFragment
{
    /**
     * @var string transaction_id
     */
    private $id;

    /**
     * @var string out_trade_no
     */
    private $refno;

    /**
     * @var int total_fee
     */
    private $total;

    /**
     * fee_type - ISO 4217 currency code
     *
     * @var string|null
     */
    private $currencyCode;

    /**
     * @var int total_refund_count|refund_count
     */
    private $count;

    /**
     * @var Refund[]
     */
    private $elements;

    /**
     * Constructor.
     *
     * @param string $id
     * @param string $refno
     * @param int    $total
     * @param int    $count
     * @param array  $elements
     */
    public function __construct(string $id, string $refno, int $total, int $count, array $elements = [])
    {
        $this->id = $id;
        $this->refno = $refno;
        $this->total = $total;
        $this->count = $count;
        $this->elements = $elements;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefno(): string
    {
        return $this->refno;
    }

    /**
     * @param string $refno
     *
     * @return $this
     */
    public function setRefno(string $refno): self
    {
        $this->refno = $refno;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string|null $currencyCode
     *
     * @return $this
     */
    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return $this
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return Refund[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param Refund[] $elements
     *
     * @return $this
     */
    public function setElements(array $elements): self
    {
        $this->elements = $elements;

        return $this;
    }
}
