<?php

/*
 * TransferException.php
 */

namespace AzureSpring\Wxpay\Exception;

class TransferException extends \RuntimeException implements WxpayException {}
