<?php

/*
 * SignatureException.php
 */

namespace AzureSpring\Wxpay\Exception;

class SignatureException extends \RuntimeException implements WxpayException {}
