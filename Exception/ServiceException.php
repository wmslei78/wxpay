<?php

/*
 * ServiceException.php
 */

namespace AzureSpring\Wxpay\Exception;

use Throwable;

class ServiceException extends \RuntimeException implements WxpayException
{
    protected $code;

    public function __construct($message = '', $code = '', Throwable $previous = null)
    {
        parent::__construct($message, -1, $previous);

        $this->code = $code;
    }
}
